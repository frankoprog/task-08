package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.constants.XML;
import com.epam.rd.java.basic.task8.entity.Monitor;
import com.epam.rd.java.basic.task8.entity.Part;
import org.xml.sax.helpers.DefaultHandler;


import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.*;
import javax.xml.transform.stream.StreamSource;
/**
 * Controller for StAX parser.
 *
 */
public class STAXController extends DefaultHandler {

	private String xmlFileName;

	// main container
	private Monitor monitor;

	public Monitor getMonitor() {
		return monitor;
	}

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	/**
	 * Parses XML document with StAX (based on event reader). There is no validation during the
	 * parsing.
	 */
	public void parse() throws XMLStreamException {

		Part part = null;

		// current element name holder
		String currentElement = null;

		XMLInputFactory factory = XMLInputFactory.newInstance();

		factory.setProperty(XMLInputFactory.IS_NAMESPACE_AWARE, true);

		XMLEventReader reader = factory.createXMLEventReader(
				new StreamSource(xmlFileName));

		while (reader.hasNext()) {
			XMLEvent event = reader.nextEvent();

			// skip any empty content
			if (event.isCharacters() && event.asCharacters().isWhiteSpace()) {
				continue;
			}

			// handler for start tags
			if (event.isStartElement()) {
				StartElement startElement = event.asStartElement();
				currentElement = startElement.getName().getLocalPart();

				if (XML.MONITOR.equalsTo(currentElement)) {
					monitor = new Monitor();
					continue;
				}

				if (XML.PART.equalsTo(currentElement)) {
					part = new Part();
					Attribute attribute = startElement.getAttributeByName(
							new QName(XML.AFFORDABLE.value()));
					if (attribute != null) {
						part.setAffordable(Boolean.parseBoolean(attribute.getValue()));
					}
				}
			}

			// handler for contents
			if (event.isCharacters()) {
				Characters characters = event.asCharacters();
				try {
					if (XML.NAME.equalsTo(currentElement)) {
						part.setName(characters.getData());
						continue;
					}

					if (XML.ORIGIN.equalsTo(currentElement)) {
						part.setOrigin(characters.getData());
						continue;
					}

					if (XML.PRICE.equalsTo(currentElement)) {
						part.setPrice(Integer.valueOf(characters.getData()));
						continue;
					}

					if (XML.MATRIX.equalsTo(currentElement)) {
						part.setMatrix(characters.getData());
						continue;
					}
				} catch(NullPointerException e) {
					//exeption
				}
			}

			// handler for end tags
			if (event.isEndElement()) {
				EndElement endElement = event.asEndElement();
				String localName = endElement.getName().getLocalPart();

				if (XML.PART.equalsTo(localName)) {
					// just add question to container
					monitor.getParts().add(part);
				}
			}
		}
		reader.close();
	}

}