package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.entity.Monitor;
import com.epam.rd.java.basic.task8.util.Sorter;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		String xmlFileName = args[0];

		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////

		// get
		DOMController domController = new DOMController(xmlFileName);
		domController.parse(true);
		Monitor monitor = domController.getMonitor();

		// sort (case 1)
		Sorter.sortPartsByName(monitor);

		// save
		String outputXmlFile = "output.dom.xml";
		DOMController.saveToXML(monitor, outputXmlFile);

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////

		// get
		SAXController saxController = new SAXController(xmlFileName);
		saxController.parse(true);
		monitor = saxController.getMonitor();

		// sort  (case 2)
		Sorter.sortPartsByOrigin(monitor);

		// save
		outputXmlFile = "output.sax.xml";

		// other way:
		DOMController.saveToXML(monitor, outputXmlFile);

		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////

		// get
		STAXController staxController = new STAXController(xmlFileName);
		staxController.parse();
		monitor = staxController.getMonitor();

		// sort  (case 3)
		Sorter.sortPartsByPrice(monitor);

		// save
		outputXmlFile = "output.stax.xml";
		DOMController.saveToXML(monitor, outputXmlFile);
	}

}
