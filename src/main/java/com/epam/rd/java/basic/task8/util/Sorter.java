package com.epam.rd.java.basic.task8.util;


import com.epam.rd.java.basic.task8.entity.Monitor;
import com.epam.rd.java.basic.task8.entity.Part;

import java.util.Collections;
import java.util.Comparator;

public class Sorter {

    private Sorter(){
        //this is constr
    }

    /**
     * Sorts parts by Origin
     */
    public static final Comparator<Part> SORT_PARTS_BY_ORIGIN = (Part o1, Part o2) -> o1.getOrigin().compareTo(o2.getOrigin());

    /**
     * Sorts parts by Name
     */
    public static final Comparator<Part> SORT_PARTS_BY_NAME = (Part o1, Part o2) -> o1.getName().compareTo(o2.getName());

    /**
     * Sorts parts by Price
     */
    public static final Comparator<Part> SORT_PARTS_BY_PRICE = (Part o1, Part o2) -> -(o2.getPrice()-o1.getPrice());

    // //////////////////////////////////////////////////////////
    // these methods take Monitor object and sort it
    // with according comparator
    // //////////////////////////////////////////////////////////

    public static final void sortPartsByName(Monitor monitor) {
        Collections.sort(monitor.getParts(), SORT_PARTS_BY_NAME);
    }

    public static final void sortPartsByOrigin(Monitor monitor) {
        Collections.sort(monitor.getParts(), SORT_PARTS_BY_ORIGIN);
    }

    public static final void sortPartsByPrice(Monitor monitor) {
        Collections.sort(monitor.getParts(), SORT_PARTS_BY_PRICE);
    }

}