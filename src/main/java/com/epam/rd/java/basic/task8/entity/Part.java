package com.epam.rd.java.basic.task8.entity;

/**
 *	Implements the Part entity.
 */
public class Part {

    private int price;

    private boolean affordable;

    private String matrix;

    private String name;

    private String origin;

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public boolean isAffordable() {
        return affordable;
    }

    public void setAffordable(boolean affordable) {
        this.affordable = affordable;
    }

    public String getMatrix() {
        return matrix;
    }

    public void setMatrix(String matrix) {
        this.matrix = matrix;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getName() + System.lineSeparator());
        sb.append(getOrigin() + System.lineSeparator());
        sb.append(getPrice() + System.lineSeparator());
        sb.append(getMatrix() + System.lineSeparator());
        sb.append(isAffordable() + System.lineSeparator());

        return sb.toString().trim();
    }

}