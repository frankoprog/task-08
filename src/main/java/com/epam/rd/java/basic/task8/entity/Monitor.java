package com.epam.rd.java.basic.task8.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * Root container. Implements the Monitor entity.
 */
public class Monitor {

    private List<Part> parts;

    public List<Part> getParts() {
        if (parts == null) {
            parts = new ArrayList<>();
        }
        return parts;
    }

    @Override
    public String toString() {
        if (parts == null || parts.isEmpty()) {
            return "Monitor containes no objects";
        }
        StringBuilder sb = new StringBuilder();
        for (Part part : parts) {
            sb.append(part + System.lineSeparator());
        }
        return sb.toString().trim();
    }

}
